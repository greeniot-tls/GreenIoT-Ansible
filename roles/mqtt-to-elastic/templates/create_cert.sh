#! /bin/bash
echo $1
openssl genrsa -rand /dev/urandom -out $1.key 2048
if [ "$1" == "ca" ]; then
	openssl req -new -x509 -days 400 -key ca.key -out ca.crt -subj "/C=SE/ST=Stockholm/L=/O=CINTE1P1II2202/OU=/CN=$1/emailAddress="
else
	openssl req -new -nodes -key $1.key  -out $1.csr -subj "/C=SE/ST=Stockholm/L=/O=CINTE1P1II2202/OU=/CN=$1/emailAddress="
	openssl x509 -req -days 400 -in $1.csr -CA ca.crt -CAkey ca.key -CAcreateserial -out $1.crt
fi
